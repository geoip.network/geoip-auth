FROM python:3.9-alpine

RUN apk add --no-cache "build-base" "musl-dev" "libffi-dev" "boost-dev>1.76.0" "boost-build>1.76.0" "boost-static>1.76.0" "openssl-dev>1.1.0" "openssl-libs-static>1.1.0" "zlib-dev>1.2.0" "zlib-static>1.2.0" "rust>1.41" "cargo"
RUN pip install --upgrade pip
RUN pip install poetry
ADD ./poetry.lock /flask/poetry.lock
ADD ./pyproject.toml /flask/pyproject.toml
RUN cd flask; poetry install
ADD ./ /flask
WORKDIR /flask

EXPOSE 8080

CMD poetry run gunicorn app:app -c gunicorn_conf.py -k egg:meinheld#gunicorn_worker