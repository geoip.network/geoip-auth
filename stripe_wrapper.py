from datetime import datetime, timedelta
from os import environ
from typing import Optional

import stripe
import stripe.error

stripe.set_app_info(
    environ.get("GEOIP_STRIPE_APP_NAME"),
    version="0.0.1",
    url=environ.get("GEOIP_STRIPE_APP_URL"),
)

stripe.api_version = "2020-08-27"
stripe.api_key = environ.get("GEOIP_STRIPE_SECRET_KEY")
stripe_public_key = environ.get("GEOIP_STRIPE_PUBLIC_KEY")

clean_pricing = []
pull_ts = datetime(day=1, month=1, year=2021)


def get_prices():
    global pull_ts, clean_pricing
    if (datetime.now()-timedelta(hours=1)) > pull_ts:
        clean_pricing = [
            (price["unit_amount"], price["nickname"], price["id"])
            for price in stripe.Price.list(product=environ.get("GEOIP_STRIPE_PRODUCT_ID"))
        ]
        clean_pricing.sort()
        pull_ts = datetime.now()
    return clean_pricing


def get_subscription(stripe_subscription_id: str, stripe_customer_id: str) -> Optional[stripe.Subscription]:
    try:
        active_subscription = stripe.Subscription.retrieve(stripe_subscription_id)
    except stripe.error.InvalidRequestError:
        active_subscription = None
    if active_subscription and active_subscription.status in ["canceled", "ended"]:
        active_subscription = None
    if active_subscription is None:
        for price in get_prices():
            subscriptions = stripe.Subscription.list(
                customer=stripe_customer_id, status="all", price=price[2]
            )
            if len(subscriptions["data"]) != 0:
                for subscription_data in subscriptions["data"]:
                    if subscription_data["status"] not in ["ended", "canceled"]:
                        active_subscription = subscription_data
                        break
    return active_subscription
