import json
import os
import datetime
import re
import uuid
from base64 import urlsafe_b64decode, b64decode, b64encode
from functools import lru_cache, wraps
from hmac import compare_digest
from ipaddress import ip_interface
from time import perf_counter, sleep
from urllib.parse import urljoin

import bleach as bleach
import nacl.pwhash
import nacl.exceptions
import nacl.signing
import nacl.utils
import nacl.hash
import nacl.encoding
import requests
from flask_redoc import Redoc
from jose import jwt
from flask import Flask, request, jsonify, _request_ctx_stack
from flask_limiter import Limiter
from flask_limiter.util import get_ipaddr
from netifaces import interfaces, ifaddresses, AF_INET, AF_INET6
from pony.orm import db_session, commit

from models import db, Login, Bearer, Session, Role
from tokens import generate_token
from users import get_user, get_stripe

db.bind(provider=os.environ.get("DATABASE_PROVIDER"), user=os.environ.get("DATABASE_USER"), password=os.environ.get("DATABASE_PASSWORD"), host=os.environ.get("DATABASE_HOST"), database=os.environ.get("DATABASE_NAME"))
app = Flask(__name__)

redoc = Redoc(app, "static/OpenAPI.yaml")

M2MAUTH_DOMAIN = os.environ.get("GEOIP_INTERNAL_AUTH_URL")
API_AUDIENCE = os.environ.get("GEOIP_URL")
ALGORITHMS = ["ED25519"]
CERTS = requests.get(urljoin(M2MAUTH_DOMAIN, "/v1.0/certs")).json()  # nosemgrep
KEYS = {
    int(key): {
        "key": nacl.signing.VerifyKey(
            val["public_key"], encoder=nacl.signing.encoding.URLSafeBase64Encoder
        ),
        "issuer": val["issuer"],
        "refresh": val["refresh"],
    }
    for key, val in CERTS.items()
}


limiter = Limiter(
    app, key_func=get_ipaddr, default_limits=["12000 per day", "1000 per hour"]
)

if os.path.isfile("secrets/ed25519.priv"):
    with open("secrets/ed25519.priv", "rb") as f:
        sk = nacl.signing.SigningKey(f.read())
else:
    with open("secrets/ed25519.priv", "wb") as f:
        sk = nacl.signing.SigningKey.generate()
        f.write(sk.encode())
    with open("secrets/ed25519.pub", "wb") as f:
        f.write(sk.verify_key.encode())


# Error handler
class AuthError(Exception):
    def __init__(self, error, status_code):
        self.error = error
        self.status_code = status_code


@app.errorhandler(AuthError)
def handle_auth_error(ex):
    response = jsonify(ex.error)
    response.status_code = ex.status_code
    return response


# Format error response and append status code
def get_token_auth_header():
    """
    Obtains the Access Token from the Authorization Header
    """
    auth = request.headers.get("Authorization", None)
    if not auth:
        raise AuthError(
            {
                "code": "authorization_header_missing",
                "description": "Authorization header is expected",
            },
            401,
        )

    parts = auth.split()

    if parts[0].lower() != "bearer":
        raise AuthError(
            {
                "code": "invalid_header",
                "description": "Authorization header must start with" " Bearer",
            },
            401,
        )
    elif len(parts) == 1:
        raise AuthError(
            {"code": "invalid_header", "description": "Token not found"}, 401
        )
    elif len(parts) > 2:
        raise AuthError(
            {
                "code": "invalid_header",
                "description": "Authorization header must be" " Bearer token",
            },
            401,
        )

    token = parts[1]
    return token


def requires_auth(f):
    """Determines if the Access Token is valid"""

    @wraps(f)
    def decorated(*args, **kwargs):
        token = get_token_auth_header()
        unverified_header = jwt.get_unverified_header(token)
        key = KEYS.get(unverified_header["kid"])
        if key is not None:
            try:
                chunks = token.split(".")
                signature = nacl.encoding.URLSafeBase64Encoder.decode(
                    chunks[2].encode()
                )
                key["key"].verify(".".join(chunks[:2]).encode(), signature)
            except nacl.exceptions.BadSignatureError as e:
                raise AuthError(
                    {
                        "code": "invalid_signature",
                        "description": "signature could not be verified",
                    },
                    401,
                ) from e
            claims = jwt.get_unverified_claims(token)
            try:
                if (
                    datetime.datetime.fromtimestamp(claims["exp"])
                    < datetime.datetime.now()
                ):
                    raise AuthError(
                        {"code": "token_expired", "description": "token is expired"},
                        401,
                    )
                r = re.compile(r"(https?://)?" + re.escape(API_AUDIENCE) + "/?")
                if (r.match(claims["aud"]) is None) and (
                    claims["iss"] != M2MAUTH_DOMAIN
                ):
                    raise AuthError(
                        {
                            "code": "invalid_claims",
                            "description": "incorrect claims,"
                            "please check the audience and issuer",
                        },
                        401,
                    )
            except KeyError as e:
                raise AuthError(
                    {
                        "code": "invalid_header",
                        "description": "Unable to parse authentication" " token.",
                    },
                    401,
                ) from e
            _request_ctx_stack.top.current_user = claims
            return f(*args, **kwargs)
        raise AuthError(
            {"code": "invalid_header", "description": "Unable to find appropriate key"},
            401,
        )

    return decorated


def requires_scope(required_scope):
    """Determines if the required scope is present in the Access Token
    Args:
        required_scope (str): The scope required to access the resource
    """
    token = get_token_auth_header()
    unverified_claims = jwt.get_unverified_claims(token)
    if unverified_claims.get("scope"):
        token_scopes = unverified_claims["scope"].split()
        for token_scope in token_scopes:
            if token_scope == required_scope:
                return True
    return False


@app.route('/v1.0/certs', methods=["GET"])
def get_certs():
    response = {
        1: {
            "public_key": sk.verify_key.encode(nacl.signing.encoding.Base64Encoder).decode(),
            "issuer": os.environ.get('GEOIP_URL'),
            "refresh": 86400
        }
    }
    return response, 200


@app.route("/v1.0/login", methods=["POST"])
def login_post():
    s = perf_counter()
    response = {}, 401
    if request.authorization is not None and request.authorization.type == "basic":
        response = basic_auth()
    elif request.headers.get("X-RENEW-KEY") is not None and request.cookies.get(
        "GEOIP_SESSION"
    ):
        response = renew(
            request.headers.get("X-RENEW-KEY"), request.cookies.get("GEOIP_SESSION")
        )
    e = perf_counter()
    sleep(max(0.1 - (e - s), 0))
    return response


@app.route("/v1.0/validate_session", methods=["POST"])
@requires_auth
def validate_session():
    response = {}, 401
    if requires_scope("validate:dynamic"):
        jdata = request.get_json()
        if jdata is not None and "session" in jdata:
            response = session_auth(jdata["session"])
    return response


@app.route("/v1.0/validate_bearer", methods=["POST"])
@requires_auth
def validate_bearer():
    response = {}, 401
    if requires_scope("validate:bearer"):
        jdata = request.get_json()
        if jdata is not None and "bearer" in jdata:
            response = bearer_auth(urlsafe_b64decode(jdata["bearer"].encode()))
    return response


@app.route("/v1.0/dynamic", methods=["POST"])
@requires_auth
def create_dynamic():
    if not requires_scope("create:dynamic"):
        return {}, 401
    user = get_user(auth0_id=request.json.get("auth0_id", ""))
    if user is None:
        return {}, 404
    stripe = get_stripe(user)
    if (
            (request.json.get("role") == "sponsor")
            and
            (stripe is not None)
            and
            stripe.active
    ):
        with db_session():
            while True:
                token = generate_token(user.auth0_id)
                username = token[16:32]
                password = token[:16]
                if Login.get(username=username) is not None:
                    continue
            Login(
                user=user,
                application_name=bleach.clean(request.json["application_name"]).encode(),
                username=username,
                password=nacl.pwhash.str(password),
                timestamp=datetime.datetime.now()
            )
            commit()
        return {
            "username": username.decode(),
            "password": password.decode(),
        }, 201
    return {}, 401


@app.route("/v1.0/bearer", methods=["POST"])
@requires_auth
def create_bearer():
    if not requires_scope("create:bearer"):
        return {}, 401
    user = get_user(auth0_id=request.json.get("auth0_id", ""))
    if user is None:
        return {}, 404
    stripe = get_stripe(user)
    if (
            (request.json["role"] == "sponsor")
            and
            (stripe is not None)
            and
            stripe.active
    ):
        with db_session():
            while True:
                token = generate_token(user.auth0_id)
                if Bearer.get(token=token) is not None:
                    continue
            Bearer(
                user=user,
                application_name=bleach.clean(request.json["application_name"]).encode(),
                token=token,
                ttl=int(request.json.get("ttl", -1))
            )
            commit()
        return {"token": token.decode()}, 201
    return {}, 401


@app.route("/v1.0/bearer", methods=["DELETE"])
@requires_auth
def delete_bearer():
    if not requires_scope("delete:bearer"):
        return {}, 401
    auth0_id = request.json.get("auth0_id", "")
    user = get_user(auth0_id=auth0_id)
    if user is None:
        return {}, 400
    application_name = bleach.clean(request.json.get("application_name", ""))
    with db_session():
        bearer = Bearer.get(user=user, application_name=application_name)
        if bearer is None:
            return {}, 404
        del bearer
        commit()
    return {}, 201


@app.route("/v1.0/dynamic", methods=["DELETE"])
@requires_auth
def delete_dynamic():
    if not requires_scope("delete:bearer"):
        return {}, 401
    auth0_id = request.json.get("auth0_id", "")
    user = get_user(auth0_id=auth0_id)
    if user is None:
        return {}, 400
    username = bleach.clean(request.json.get("username", ""))
    with db_session():
        login = Login.get(user=user, username=username)
        if login is None:
            return {}, 404
        del login
        commit()
    return {}, 201


@app.route("/v1.0/dynamic/<path:auth0_id>", methods=["GET"])
@requires_auth
def list_dynamic(auth0_id):
    if not requires_scope("list:dynamic"):
        return {}, 401
    user = get_user(auth0_id=auth0_id)
    if user is None:
        return {}, 400
    dynamics = []
    with db_session():
        for login in user.logins:
            login_dict = {
                "username": login.username,
                "application_name": login.application_name,
                "timestamp": login.timestamp,
            }
            dynamics.append(login_dict)
    return jsonify(dynamics)


@app.route("/v1.0/bearer/<path:auth0_id>", methods=["GET"])
@requires_auth
def list_bearer(auth0_id):
    if not requires_scope("list:bearer"):
        return {}, 401
    user = get_user(auth0_id=auth0_id)
    if user is None:
        return {}, 400
    bearers = []
    with db_session():
        for bearer in user.bearers:
            bearer_dict = {
                "application_name": bearer.application_name,
                "timestamp": bearer.timestamp,
            }
            bearers.append(bearer_dict)
    return jsonify(bearers)


def basic_auth():
    timestamp = datetime.datetime.now()
    if request.authorization.username in [None, ""]:
        return {}, 401
    with db_session():
        login = Login.get(username=request.authorization.username)
        if login is None:
            return {}, 401
        try:
            nacl.pwhash.verify(login.password, request.authorization.password.encode())
            stripe = get_stripe(login.user)
            if stripe.active:
                role = Role.sponsor
            else:
                role = Role.user
            return create_auth_response(login, role, timestamp)
        except nacl.exceptions.InvalidkeyError:
            return {}, 401


def bearer_auth(bearer_token: bytes):
    if bearer_token in [None, ""]:
        return {}, 401
    with db_session():
        bearer = Bearer.get(token=bearer_token)
        if bearer is None:
            return {}, 401
        stripe = get_stripe(bearer.user)
        if stripe.active:
            role = Role.sponsor
        else:
            role = Role.user
        return (
            create_jwt(
                bearer.application_name,
                b"BEARER",
                bearer.auth0_id,
                role,
                bearer.timestamp,
                bearer.ttl,
            ),
            200,
        )


def session_auth(enc_session_jwt):
    header, payload = decrypt_jwt(enc_session_jwt)
    session_key = bleach.clean(payload.get("session_key", ''))
    with db_session:
        session = Session.get(session_key=session_key)
        if session is None:
            return {}, 401
        login = session.login
        user = login.user
        stripe = get_stripe(user)
        if stripe.active:
            role = Role.sponsor
        else:
            role = Role.user
        return (
            create_jwt(
                user.application_name,
                session.session_key,
                user.auth0_id,
                role,
                login.timestamp,
                session.ttl,
            ),
            200,
        )


def renew(refresh_key: str, enc_session: str):
    timestamp = datetime.datetime.now()
    header, payload = decrypt_jwt(enc_session)
    session_key = bleach.clean(payload.get("session_key", ''))
    with db_session():
        session = Session.get(session_key=session_key)
        if session is None:
            return {}, 401
        login = session.login
        user = login.user
        stripe = get_stripe(user)
        if stripe.active:
            role = Role.sponsor
        else:
            role = Role.user
        if not (
                (session.timestamp + datetime.timedelta(seconds=session.ttl) >= timestamp)
                and
                compare_digest(session.refresh_key, b64decode(refresh_key))
        ):
            return {}, 401
        regenerated_session = uuid.uuid5(
            uuid.NAMESPACE_URL,
            f"sessionkey;{user.username};{payload['timestamp']}"
        ).bytes_le
        if not compare_digest(session.session_key, regenerated_session):
            return {}, 401
        return create_auth_response(login, role, timestamp)


def decrypt_jwt(enc_jwt):
    jwt = sk.verify_key.verify(enc_jwt, encoder=nacl.signing.encoding.URLSafeBase64Encoder)
    header_b64, payload_b64 = jwt.split(b".")
    payload = json.loads(b64decode(payload_b64).decode())
    header = json.loads(b64decode(header_b64).decode())
    return header, payload


def create_auth_response(login: Login, role: Role, timestamp: datetime):
    with db_session():
        session = Session(
            session_key=uuid.uuid5(
                uuid.NAMESPACE_URL, f"sessionkey;{login.username};{timestamp}"
            ).bytes_le,
            refresh_key=uuid.uuid5(
                uuid.NAMESPACE_URL, f"requestkey;{login.username};{timestamp}"
            ).bytes_le,
            ttl=3600,
            timestamp=timestamp.timestamp(),
            username=login.username,
            role=role
        )
        commit()
    jwt = create_jwt(
        login.application_name,
        session.session_key,
        login.auth0_id,
        role,
        timestamp.timestamp(),
        session.ttl,
    )
    response = jsonify(
        {
            "refresh": b64encode(session.refresh_key).decode(),
            "ttl": 3600,
        }
    )
    response.set_cookie(
        "GEOIP_SESSION",
        jwt.decode(),
        max_age=3600,
        domain="geoip.network",
        httponly=True,
        samesite="Strict",
    )
    response.status_code = 200
    return response


def create_jwt(application_name, session_key, auth_id, role, timestamp, ttl):
    header = b64encode(
        json.dumps(
            {
                "typ": "JWT",
                "alg": "ED25519",
            }
        ).encode()
    )
    payload = b64encode(
        json.dumps(
            {
                "session_key": b64encode(session_key).decode(),
                "application_name": application_name.decode(),
                "auth_id": auth_id,
                "role": role,
                "timestamp": timestamp,
                "ttl": ttl,
            }
        ).encode()
    )
    enc_signature = sk.sign(f"{header.decode()}.{payload.decode()}".encode(), encoder=nacl.signing.encoding.URLSafeBase64Encoder)
    return enc_signature


@lru_cache(maxsize=None)
def local_addresses():
    ip_list = []
    for interface in interfaces():
        for link in ifaddresses(interface).get(AF_INET, []):
            ip_list.append(ip_interface(f"{link['addr']}/{link['netmask']}").network)
    for interface in interfaces():
        for link in ifaddresses(interface).get(AF_INET6, []):
            ip_list.append(
                ip_interface(
                    f"{link['addr'].split('%')[0]}/{link['netmask'].split('/')[1]}"
                ).network
            )
    return ip_list


if __name__ == "__main__":
    app.run()
