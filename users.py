from datetime import datetime, timedelta
from typing import Optional

from pony.orm import db_session, commit

import stripe_wrapper
from models import User, Stripe


def get_user(uid: int = None, auth0_id: str = None) -> Optional[User]:
    with db_session():
        if None not in [uid, auth0_id]:
            user = User.get(uid=uid, auth0_id=auth0_id)
        elif uid is not None:
            user = User.get(uid=uid)
        elif auth0_id is not None:
            user = User.get(auth0_id=auth0_id)
        else:
            raise KeyError("invalid input")
    return user


def get_stripe(user: User) -> Optional[Stripe]:
    with db_session():
        stripe = user.stripe
        if (
                (stripe is not None)
                and
                (stripe.timestamp <= (datetime.now() - timedelta(days=1)))
        ):
            subscription = stripe_wrapper.get_subscription(
                stripe.subscription_id, stripe.customer_id
            )
            if subscription is None:
                stripe.active = False
            else:
                stripe.subscription_id = subscription.id
                stripe.active = True
            stripe.timestamp = datetime.now()
            commit()
    return stripe
