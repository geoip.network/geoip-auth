from datetime import datetime

import nacl


def generate_token(auth0_id: str):
    return nacl.hash.sha256(
        f'{auth0_id}-{int(datetime.now().timestamp())}'.encode(),
        nacl.encoding.Base64Encoder,
    )