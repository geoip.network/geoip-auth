from datetime import datetime
from enum import IntEnum

from pony.orm import *


db = Database()


class Role(IntEnum):
    user = 0
    sponsor = 10
    isp = 25
    service = 50  # Deprecated
    admin = 100  # Deprecated


class User(db.Entity):
    uid = PrimaryKey(int, auto=True)
    auth0_id = Required(str, unique=True)
    logins = Set("Login")
    bearers = Set("Bearer")
    stripe = Optional("Stripe")


class Stripe(db.Entity):
    uid = PrimaryKey(int, auto=True)
    subscription_id = Required(str)
    customer_id = Required(str)
    active = Optional(bool)
    timestamp = Required(datetime)


class Login(db.Entity):
    uid = PrimaryKey(int, auto=True)
    user = Required(User)
    application_name = Required(str)
    username = Required(str, unique=True)
    password = Required(bytes)
    timestamp = Required(datetime)


class Bearer(db.Entity):
    uid = PrimaryKey(int, auto=True)
    user = Required(User)
    application_name = Required(str)
    token = Required(bytes, unique=True)
    ttl = Required(int)
    timestamp = Required(datetime)


class Session(db.Entity):
    uid = PrimaryKey(int, auto=True)
    session_key = Required(bytes)
    refresh_key = Required(bytes)
    ttl = Required(int)
    timestamp = Required(datetime)
    login = Login

